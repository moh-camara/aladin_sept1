import { Component, Input, OnInit,OnChanges,Output,EventEmitter} from '@angular/core';
import { charAtIndex } from 'pdf-lib';
import { LocalService } from 'src/app/core';
declare  var require:any;
var myalert=require('sweetalert2');
var $ = require("jquery");

@Component({
  selector: 'app-aladin-designs-packs',
  templateUrl: './aladin-designs-packs.component.html',
  styleUrls: ['./aladin-designs-packs.component.scss']
})

export class AladinDesignsPacksComponent implements OnInit {
@Input() data:any;
@Input() newdata:any;
@Output () newItemEvent = new EventEmitter<boolean>()
price:any=0;
unit_price=0;

mydesigns:any=[];
inpnmb =1;
qty=100;
prod:any=[];
active_prod:any;
hide_calco=true;
show_modal=true;
active=false;
shape:any;
type:any;
reste="";
price_1=[5000,5000,7000,7000,10000,15000,15000];
// size=["20/30","30/30","33/40","40/45","50/60"];
size_sac=["15/20","20/25","25/30","28/35","35/45","45/50","50/55"];
size={
  s1:false,
  s2:false,
  s3:false,
  s4:false,
  s5:false,
  s6:false,
  seri:false,

}
  constructor(private local:LocalService) { }

  ngOnInit(): void {
    $("body").children().first().before($(".modal"));


  }


  OnChanges(){
    this.mydesigns = this.local.items;
    if(this.mydesigns.length>0){
      for(let item of this.mydesigns){
        if(item.category=="packs"){
          this.prod.push(item)
        }
      }
    }
  }

  getPrice(){
    let price=this.unit_price;
    this.inpnmb =+this.inpnmb+1;
      if(this.inpnmb>0&&price!=null){
        this.price= this.inpnmb*(+price);
      }
      this.qty=this.inpnmb*100;
    }

  getPricem(){
    let price=this.unit_price
    this.inpnmb=this.inpnmb-1;
    if(this.inpnmb>0 && price!=null){
      if(((+this.inpnmb)*(+this.price))>0){
        this.price= ((+this.inpnmb)*(+price));
      }
      this.qty=this.qty-100
  }else{
    this.inpnmb=this.inpnmb+1;

  }
    }

    decremet_by(){
      let price=this.unit_price
      this.inpnmb =(+this.inpnmb)*2;
      if(this.inpnmb>0&&price!=null){
        this.price= this.inpnmb*(+price);
      }
      this.qty=this.qty*2
    }


getProdprice=(event:any)=>{
  console.log(event.target.name)
  this.unit_price=parseInt(event.target.name)
  this.price=this.unit_price
  this.inpnmb=1;
  this.active_prod=event.target.id;
  console.log(event.target.id);
  this.getId(this.active_prod);
}


  getId=(id:any)=>{
    if(id!=-1){
      return this.mydesigns[+id];
    }else{
      return -1;
    }

  }


  // removeOneItem(event:any){
  //   let id=event.target.title;
  //   console.log(id)
  //   if(+id!=-1){
  //     this.local.removeOne(id);
  //   }

  // }

  divide(){
    let price=this.unit_price

    console.log(this.inpnmb);
    if ((this.inpnmb%2)==0){

      if(this.inpnmb>1 && price!=null){
      this.inpnmb=(+this.inpnmb)/2;
      this.price=this.price/2;
      this.qty= this.qty / 2


      // if(((+this.inpnmb)/(+this.price))>0){
      // }
      }
      if(this.inpnmb<1){
      this.qty=this.qty;
      this.price=this.price;
      console.log(this.qty);

      }
    }else{
      console.log(this.inpnmb)
    }



  }

  hidemodal(){
    this.hide_calco=!this.hide_calco;
    console.log(this.size);
  }

  removeOneItem(event:any){
    let id=event.target.title;
    let cart = charAtIndex(id,2);
    let prod=charAtIndex(id,0);
    if(+cart[0]!=-1){
      this.local.removeOne(id);
    if(this.prod.length==1){
      this.prod=[]
    location.reload();

    }else{
      this.prod.splice(+prod[0],+prod[0]);

    }


    }

  }

  removeItem(id:any){
    if(id!=-1){
      this.local.removeOne(id);
    }
  }

  addtocart(event:any){
    let data = this.getId(this.active_prod);
   if(data!=-1){
      let cart_data={
        qty:this.inpnmb,
        price:this.unit_price,
        t:this.price,
        id:data.prod.id,
        face1:data.face1,
        face2:data.face2,
        text1:data.text1,
        text2:data.text2,
        shape:this.shape,
        type:this.type,
        size:this.size,
        name:data.prod.name,
        production_f1:data.production_f1,
        production_f2:data.production_f2,
        type_product:"editor"

       };

       this.local.adtocart(cart_data);
        myalert.fire({
          title:'<strong>produit ajouté</strong>',
          icon:'success',
          html:
            '<h6 style="color:blue">Felicitation</h6> ' +
            '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
            '<a href="/cart">Je consulte mon panier</a>'
            ,
          showCloseButton: true,
          focusConfirm: false,

        })
        this.removeItem(this.active_prod);
        location.reload();


    }else{
      myalert.fire({
        title: "Erreur",
        text: "Une erreur s'est produite",
        icon: "error",
         button: "Ok"
        })
    }
  }


}
