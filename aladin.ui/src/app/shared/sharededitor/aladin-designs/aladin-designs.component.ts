import { Component, Input, OnInit ,OnChanges} from '@angular/core';
import { LocalService } from 'src/app/core';
@Component({
  selector: 'app-aladin-designs',
  templateUrl: './aladin-designs.component.html',
  styleUrls: ['./aladin-designs.component.scss']
})
export class AladinDesignsComponent implements OnInit,OnChanges {
@Input() data:any;
@Input() newpackdata:any;
packs:any=[];
clothes:any=[];
mydesigns :any=[];
constructor(private localservice:LocalService) { }
  ngOnChanges():void{
    this.mydesigns = this.localservice.items;  
    if(this.mydesigns.length>0){
      if(this.packs.length==0){
        for(let item of this.mydesigns){
          if(item.category=="packs"){
            this.packs.push(item)
          }
          if(item.category=="clothes"){
            this.clothes.push(item)
  
          }
        }
      }else{
        this.packs.push(this.mydesigns[this.mydesigns.length-1]);

      }
    }
  }

 
  ngOnInit(): void {
   

  }

}
