import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-aladin-faces',
  templateUrl: './aladin-faces.component.html',
  styleUrls: ['./aladin-faces.component.scss']
})
export class AladinFacesComponent implements OnInit {
  Front="imgfront";
  Back="imgback";

@Input() URL:any;
@Input() face1:any;
@Input() back:any;
@Input() front:any;
@Output() newItemEvent =new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }
  addNewItem(value: any) {
    this.newItemEvent.emit(value);
  }
}
