import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AladinShapesComponent } from './aladin-shapes.component';

describe('AladinShapesComponent', () => {
  let component: AladinShapesComponent;
  let fixture: ComponentFixture<AladinShapesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AladinShapesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AladinShapesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
