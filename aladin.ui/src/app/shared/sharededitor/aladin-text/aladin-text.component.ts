import { Component, OnInit ,Output,EventEmitter} from '@angular/core';
import { ColorEvent } from 'ngx-color';

@Component({
  selector: 'app-aladin-text',
  templateUrl: './aladin-text.component.html',
  styleUrls: ['./aladin-text.component.scss']
})
export class AladinTextComponent implements OnInit {
  hide=false;
  hide1=false;
  hide2=false;
  hide3=false
  Text:any;
  textght:any;
  textwdt=40;
  strokewidth=2;
  strokeColor:any;
  fonts = ["Flowerheart","HussarBold",'Branda',"MarginDemo","Kahlil","FastHand","BigfatScript","Atlane","Bismillahscript","HidayatullahDemo","Robus","Backslash","ChristmasStory"];
   fonts_url=["./assets/fonts/FlowerheartpersonaluseRegular-AL9e2.otf","./assets/fonts/HussarBoldWebEdition-xq5O.otf","./assets/fonts/Branda-yolq.ttf","./assets/fonts/MarginDemo-7B6ZE.otf","./assets/fonts/Kahlil-YzP9L.ttf","./assets/fonts/FastHand-lgBMV.ttf","./assets/fonts/BigfatScript-jE96G.ttf","./assets/fonts/Atlane-PK3r7.otf","./assets/fonts/Bismillahscript-4ByyY.ttf","./assets/fonts/HidayatullahDemo-mLp39.ttf","./assets/fonts/Robus-BWqOd.otf","./assets/fonts/Backslash-RpJol.otf","./assets/fonts/ChristmasStory-3zXXy.ttf"]
  @Output() newTextColorEvent = new EventEmitter<ColorEvent>();
  @Output() newTextFontEvent = new EventEmitter<any>();
  @Output() newTextUnderlineEvent=new EventEmitter<string>();
  @Output() newTextEvent=new EventEmitter<string>();
  @Output() newTextItalicEvent=new EventEmitter<string>();
  @Output() newTextBoldEvent=new EventEmitter<string>();
  @Output() newTextHeightEvent = new EventEmitter<string>();
  @Output() newTextWidthtEvent = new EventEmitter<string>();
  @Output() newTextReturnEvent = new EventEmitter<string>();
  @Output() newTextCopyEvent = new EventEmitter<string>();
  @Output() newTextPasteEvent = new EventEmitter<string>();
  @Output() newTextRemoveEvent=new EventEmitter<string>();
  @Output() newTextOverlineEvent=new EventEmitter<any>();
  @Output() newTextMinusEvent=new EventEmitter<any>();
  @Output() newTextPlusEvent=new EventEmitter<any>();
  @Output() newTextStrokeEvent=new EventEmitter<any>();
@Output() newshadowingEvent=new EventEmitter<any>();
colorarray=['#D9E3F0', '#F47373', '#697689', '#37D67A', '#2CCCE4', '#555555',"#D2691E",'#dce775', '#ff8a65', '#ba68c8',"blue","yellow","orange","blacK","red","indigo","green","brown","#800080","#808000","#000080"];

  constructor() { }

  ngOnInit(): void {
  }

  cache3(){
    this.hide3=!this.hide3
    this.hide=false
    this.hide1=false
    this.hide2=false
  }
   cache(){
    this.hide=!this.hide
    this.hide1=false
    this.hide3=false
   }
  cache1(){
    this.hide1=!this.hide1
    this.hide=false
    this.hide3=false
  }
  cache2(){
    this.hide2=!this.hide2;
    this.hide1=false;
    this.hide=false;
    this.hide3=false;
  }
  
  textoverline(event:any){
    this.newTextOverlineEvent.emit(event);
  }
  
  selectChangeHandler(event:any) {
  for(let item of this.fonts){
    if(event.target.value==item){
      let data = {url:this.fonts_url[this.fonts.indexOf(item)],name:item}
      this.newTextFontEvent.emit(data)
      break
    }
  }
   


    
  }
  
  
  texteclor($event:ColorEvent){
    this.newTextColorEvent.emit($event)
  
  }
  
  textintalic(event:any){
    this.newTextItalicEvent.emit(event)
  }
  
  textbold(event:any){
    this.newTextBoldEvent.emit(event);
  }
  
  
  textunderline(){
    this.newTextUnderlineEvent.emit();
  }
  
  textheight(event:any){
    this.newTextHeightEvent.emit(event);
  }
  
  textwidth(event:any){
    this.newTextWidthtEvent.emit(event);
  }
    
  textreturn(){
    this.newTextReturnEvent.emit();
  }
  
  
  copy(){
  this.newTextCopyEvent.emit();
  }
  
  
  paste(){
  this.newTextPasteEvent.emit();
  }
  
  text(event:any){
    this.newTextEvent.emit(event);
  }
  
  textremove(){
    this.newTextRemoveEvent.emit(); 
  
  }
  
  minus(event:any){
    this.textwdt=+this.textwdt - 1;
  
    this.newTextMinusEvent.emit(+event-1);
  
  }
  
  plus(event:any){
    this.textwdt=+this.textwdt + 1;
    this.newTextPlusEvent.emit(+event+1);
    
  } 
  
  TextStroke(event:any){
    this.cache3()
  
  }
  
  strokecolor($event:ColorEvent){
  this.strokeColor=$event.color.hex;
  this.newTextStrokeEvent.emit({color:this.strokeColor,width:3});
  
  }
  
  shadowing($event:ColorEvent){
    let color=$event.color.hex
    this.newshadowingEvent.emit(color)
  
  }
  

}
