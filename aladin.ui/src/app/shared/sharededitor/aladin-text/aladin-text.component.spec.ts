import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AladinTextComponent } from './aladin-text.component';

describe('AladinTextComponent', () => {
  let component: AladinTextComponent;
  let fixture: ComponentFixture<AladinTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AladinTextComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AladinTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
