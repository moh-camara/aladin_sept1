import { Component, Input, OnInit,OnChanges } from '@angular/core';
import { fabric } from 'fabric';
declare var require: any
declare var FontFace:any;
var $ = require("jquery");
import { ColorEvent } from 'ngx-color';
var myalert=require('sweetalert2')
import { LocalService } from 'src/app/core';
@Component({
  selector: 'app-aladin',
  templateUrl: './aladin.component.html',
  styleUrls: ['./aladin.component.scss']
})
export class AladinComponent implements OnInit ,OnChanges{
@Input() data:any;
canvas:any;
spinner=false
objectToSendBack:any;
face1:any
face2:any
current_col:any;
cactive=true;
active=true;
origin_1=null;
origin_2=null;
front_data:any=[];
back_data:any=[];
production_f1:any;
colors:any=["indigo"];
production_f2:any;
newdatapack:any;

colorarray=['#D9E3F0', '#F47373', '#697689', '#37D67A', '#2CCCE4', '#555555',"#D2691E",'#dce775', '#ff8a65', '#ba68c8',"blue","yellow","orange","blacK","red","indigo","green","brown","#800080","#808000","#000080"];
constructor(private local:LocalService) { }

  ngOnInit(): void {
    $("body").children().first().before($(".modal"));

    this.canvas = new fabric.Canvas('aladin',{ 
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor:'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful:true
    }); 
    this.makeImage(this.data.url);

    this.canvas.filterBackend=new fabric.WebglFilterBackend();
    this.canvas.on('mouse:move', (event:any)=>{
     // this.onMouseMove(event);
    });
    this.canvas.on('mouse:down', (event:any)=>{
     // this.onMouseDown();   

    });

    this.canvas.on('mouse:out', (event:any)=>{

     // this.hide();   

    });
    this.canvas.on("selection:created", (event:any)=>{
      this.objectToSendBack = event.target;
     // this.sendSelectedObjectBack()
    }); 
    this.canvas.on("selection:updated",(event:any)=>{
      this.objectToSendBack = event.target;
    });

  }

  ngOnChanges(){
    this.makeImage(this.data.url);
    console.log(this.data.url)
    this.face1=this.data.url;
    this.face2=this.data.url;

  }

  sendSelectedObjectBack =()=>{
    this.canvas.sendToBack(this.objectToSendBack);
  }
  toggleSpinner(){
this.spinner=!this.spinner;
  }

  makeImage(url:any){
    var img = new Image();
    let canvas= this.canvas;
    img.src=url
    img.crossOrigin ='anonymous';
    if(!this.spinner){
      this.toggleSpinner()
    }
    img.onload = ()=> {
    var Img = new fabric.Image(img);
    canvas.setBackgroundImage(Img,canvas.renderAll.bind(canvas), {
            backgroundImageOpacity: 0.1,
            backgroundImageStretch: true,
            left:5,
            top:5,
            right:5,
            bottom:5,
            height:600,
            width:600,
        });
        this.toggleSpinner();
        canvas.centerObject(Img);
        canvas.renderAll(Img);
        canvas.requestRenderAll(); 
      }
  }

createText(event:any){
 let text= new fabric.IText("TEXTE",{
    top:200,
    left:200,
    fill:"blue",
    fontSize:38,
    fontStyle:'normal',
    cornerStyle:'circle',
    selectable:true,
    borderScaleFactor:1,
    overline:false
  });
  this.canvas.add(text).setActiveObject(text);
  this.canvas.renderAll(text);
  this.canvas.requestRenderAll();
  this.canvas.centerObject(text);
}

overLineText(event:any){
  let item = this.canvas.getActiveObject()
    if(item.type=="activeSelection"){
      for(let i of item){
        if(!i.overline){
          i.set({overline:true})
          this.canvas.renderAll(i);
          this.canvas.requestRenderAll()
        }else{
          i.set({overline:false});
          this.canvas.renderAll(i);
          this.canvas.requestRenderAll()
        }
      }
    }
    
    if(item.type!="activeSelection"){
        if(!item.overline){
          item.set({overline:true});
          this.canvas.renderAll(item);
          this.canvas.requestRenderAll()
        }else{
          item.set({overline:false});
          this.canvas.renderAll(item);
          this.canvas.requestRenderAll()
        }
      
    }
}

underLineText(){
  let item=this.canvas.getActiveObject()
  if(item && item.type!="activeSelection"){
    if(!item.underline){
      item.set({underline:true});
      this.canvas.renderAll(item);

    }else{
      item.set({underline:false});
      this.canvas.renderAll(item);
  
    }
  }

  if(item && item.type=="activeSelection"){

    for(let el of item._objects){
      if(!item.underline){

        el.set({underline:true});
        this.canvas.renderAll(el);

      }else{
        el.set({underline:false});
        this.canvas.renderAll(el);
      }
    }
  }
  this.canvas.requestRenderAll();
}


ItalicText(event:any){
  let item= this.canvas.getActiveObject();
  if(item!=undefined && item.type=="activeSelection"){
    for(let el of item._objects){
      if(el.fontStyle!="normal"){
        el.set({fontStyle:'normal'});
        this.canvas.renderAll(el)
       
      }else{
        el.set({fontStyle:'italic'});
        this.canvas.renderAll(el);
      }
    }
    
  }
  if(item && item!="activeSelection"){
    if(item.fontStyle!="normal"){
      item.set({fontStyle:"normal"});
      this.canvas.renderAll(item);
    }else{
      item.set({
        fontStyle:"italic"
      });
      this.canvas.renderAll(item);
    }
  }
  this.canvas.requestRenderAll();
}


boldText(event:any){
  let item= this.canvas.getActiveObject();
  if(item!=undefined && item.type=="activeSelection"){    
      for(let el of item._objects){
        if(el.fontWeight=="normal"){
        el.set({fontWeight:'bold'});
        this.canvas.renderAll(el);
      }else{
          el.set({fontWeight:'normal'});
          this.canvas.renderAll(el);        
      }
    }
    this.canvas.requestRenderAll();  
  }
    
  if(item!=undefined && item.type!="activeSelection"){
    if(item.fontWeight=="normal"){   
      item.set({fontWeight:'bold'});
        this.canvas.renderAll(item);
        this.canvas.requestRenderAll();
     
    }else{
      item.set({fontWeight:'normal'});
        this.canvas.renderAll(item);
        this.canvas.requestRenderAll();   
    }

  }
}

removeItem(){
  let item = this.canvas.getActiveObject();
  if(item && item.type!="activeSelection"){
    this.canvas.remove(item);
  }

  if(item && item.type=="activeSelection"){
    for(let e of item._objects){
      this.canvas.remove(e);
    }
  }

}

setTextBeforeEdit(){
  let text = this.canvas.getActiveObject()
  if(text && text.type!="activeSelection"){ 
    if(text.underline && text._textBeforeEdit){
      text.set({text:text._textBeforeEdit,underline:false});
      this.canvas.renderAll(text);

    }   
    if(text._textBeforeEdit){
      text.set({text:text._textBeforeEdit});
      this.canvas.renderAll(text);

    }
  } 

  if(text && text.type=="activeSelection"){
    for(let el of text._objects){
      if(el.underline && el._textBeforeEdit){
        el.set({text:el._textBeforeEdit,underline:false});
        this.canvas.renderAll(el);

      }   
      if(el._textBeforeEdit){
        el.set({text:el._textBeforeEdit});
        this.canvas.renderAll(el);
      }

    }
  }
  this.canvas.requestRenderAll();

}

Textfont(event: any) {
  console.log(event)
  const myfont :any= new FontFace(event.name,`url(${event.url})`);

  myfont.load().then((loaded:any)=>{
    (document as any).fonts.add(loaded)
    let item= this.canvas.getActiveObject();

    if( item &&  item.type=="activeSelection"){
      for(let el of  item._objects){
        el.set({
          fontFamily:event.name
        });
        this.canvas.renderAll(el);
        this.canvas.requestRenderAll();
  
      }
  
    }
  
    if( item &&  item.type!="activeSelection"){
      item.set({
        fontFamily:event.name
      });
      this.canvas.renderAll(item);
      this.canvas.requestRenderAll();
    }

  }).catch((err:any)=>{
    console.log(err);
    
  })
 
 
}


textwidth(event:any){
  let text_w =document.getElementById("R")
  let item = this.canvas.getActiveObject()
       item.set({width:+event,fontSize:+event});
        this.canvas.renderAll(item);
        this.canvas.requestRenderAll();

  text_w?.addEventListener("input",()=>{
    if(item){
      if(item.type!="activeSelection"){
        item.set({width:+event,fontSize:+event});
        this.canvas.renderAll(item);
        this.canvas.requestRenderAll();
      }
    }
    console.log(event);
});
}


textwidthminus(event:any){
  let item = this.canvas.getActiveObject()
  item.set({width:+event,fontSize:+event});
   this.canvas.renderAll(item);
   this.canvas.requestRenderAll();
}
textwidthplusOne(event:any){
  let item = this.canvas.getActiveObject()
  item.set({width:+event,fontSize:+event});
   this.canvas.renderAll(item);
   this.canvas.requestRenderAll();
}




OnRightClick(event:any){   
  let item = this.canvas.getActiveObject();
  if(item){
    var elt=document.getElementById('xdv')
   this.triggerMouse(elt)   
  }  
  return false
}


triggerMouse(event:any){
  var evt = new MouseEvent("click", {
    view: window,
    bubbles: true,
    cancelable: true,
});
event.dispatchEvent(evt);

}


changeComplete($event:ColorEvent){
  let image;
  if(this.colors.length=1){

    myalert.fire({
      title: "pas de couleur disponible",
      text: `il n'y a pas d'autre couleurs pour ce sac`,
      icon: "error",
       button: "Ok"
      });
      setTimeout(()=>{
        let idcol= document.getElementById("clr");
        this.triggerMouse(idcol)
      },1000);
  }
  for(let elt of this.data){
      if($event.color.hex==elt.lib){
        image=elt.lib_img;
        this.face1=image;
        this.face2=elt.lib_back;
        this.current_col=$event.color.hex;
        break
        }
      }
   if(image!=undefined){
    this.canvas.backgroundImage=null;
    this.makeImage(image);
    this.cactive=true;
    this.origin_1=null;
    this.origin_2=null;
    let idcol= document.getElementById("clr");
    this.triggerMouse(idcol);

  }

}



setshdow($event:any){
  console.log($event)
  let shadow = new fabric.Shadow({
    color: $event,
    blur: 20,
    offsetX: -4,
    offsetY: 3
  });

  let item =  this.canvas.getActiveObject();
  if(item && item.type!="activeSelection"){
    item.set({shadow:shadow});
    this.canvas.renderAll(item);
  }
  if(item && item.type=="activeSelection"){
    for(let el of item._objects){
      el.set({shadow:shadow});
      this.canvas.renderAll(el);
    }
  }
    this.canvas.requestRenderAll();
}

setStroke($event:any){
  let color=$event.color;
  console.log(color);
  console.log($event.width);
  let item =  this.canvas.getActiveObject();
  if(item && item.type!="activeSelection"){
    item.set({strokeWidth:1,stroke:color});
    this.canvas.renderAll(item);
  }
  if(item && item.type=="activeSelection"){
    for(let el of item._objects){
      el.set({strokeWidth:1,stroke:color});
      this.canvas.renderAll(el);
    }
  }
    this.canvas.requestRenderAll();
}




texteclor($event:ColorEvent){
  let color = $event.color.hex;   

   let item= this.canvas.getActiveObject();

   if(item && item.type!="activeSelection"){
     item.set({fill:color});
     this.canvas.renderAll(item);
   }

   if(item && item.type=="activeSelection"){
     for(let el of item._objects){
       el.set({fill:color});
       this.canvas.renderAll(el);
     }
    }
 this.canvas.requestRenderAll();

 this.changeColor(color)

}


changeColor(color:any){
  let imgInstance=this.canvas.getActiveObject()
if(this.canvas.getActiveObject()._originalElement.localName==="img"){
  this.canvas.getActiveObject()._originalElement.setAttribute('width',350)
  this.canvas.getActiveObject()._originalElement.setAttribute('height',350)
  let   filter = new fabric.Image.filters.BlendColor({
    color: color,
   });
 filter.setOptions({
mode:"tint",
alpha:0.5,
scaleX:0.5,
scaleY:0.5
 })
  imgInstance.filters.push(filter);
  imgInstance.applyFilters();
 


}
 
}

setItem(event:any){
  event.target.setAttribute("crossOrigin",'anonymous');
 event.target.setAttribute("width",200);
 event.target.setAttribute("height",200);
 var imgInstance:any = new fabric.Image(event.target);
 this.canvas.add(imgInstance).setActiveObject(imgInstance);
 this.canvas.centerObject(imgInstance);
 this.canvas.renderAll(imgInstance);
 }


 
Copy() {
  let canvas=this.canvas;
	canvas.getActiveObject().clone(function(cloned:any) {
		canvas._clipboard= cloned;
	});
}


Paste() {
  let canvas=this.canvas
	canvas._clipboard.clone(function(clonedObj:any) {
		canvas.discardActiveObject();
		clonedObj.set({
			left: clonedObj.left + 15,
			top: clonedObj.top + 15,
			evented: true,
		});
		if (clonedObj.type === 'activeSelection') {
			clonedObj.canvas = canvas;
			clonedObj.forEachObject(function(obj:any) {
				canvas.add(obj);
			});
			clonedObj.setCoords();
		} else {
			canvas.add(clonedObj);
		}
		canvas._clipboard.top += 15;
		canvas._clipboard.left += 15;
		canvas.setActiveObject(clonedObj);
		canvas.requestRenderAll();
	});
}




onFileUpload(event:any){
  let file = event.target.files[0];
  if(!this.handleChanges(file)){
    
    const reader = new FileReader();
  
   reader.onload = () => {
    let url:any = reader.result;
    fabric.Image.fromURL(url,(oImg) =>{
    oImg.set({  
     left: 100,
    top: 100,
    
    width:200,
    height:200
   });
    this.canvas.add(oImg).setActiveObject(oImg);
    this.canvas.centerObject(oImg);
    this.canvas.renderAll(oImg)
  
  })
    };
    reader.readAsDataURL(file);
    
  }
   
    }


setImage(value:any){
  this.data=value;
  this.face2=this.data.url;
  this.face1=this.data.url;
  var objt=this.canvas.getObjects()
  for(let i=0;i<objt.length;i++){
    this.canvas.remove(objt[i]);
  }
  this.canvas.backgroundImage=null;
  var img = new Image();
    let canvas= this.canvas;
    img.src=this.data.url
    img.crossOrigin ='anonymous';
    
    img.onload = ()=> {
    var Img = new fabric.Image(img);
    canvas.setBackgroundImage(Img,canvas.renderAll.bind(canvas), {
            backgroundImageOpacity: 0.1,
            backgroundImageStretch: true,
            left:5,
            top:5,
            right:5,
            bottom:5,
            height:600,
            width:600,
        });
        canvas.centerObject(Img);
        canvas.renderAll(Img);
        canvas.requestRenderAll(); 
        var elt:any=document.getElementById('closept');
        this.triggerMouse(elt); 
      }
 
  

}
    
handleChanges(file:any):Boolean {
  var fileTypes = [".png"];
  var filePath = file.name;
  if (filePath) {
               var filePic = file; 
               var fileType = filePath.slice(filePath.indexOf(".")); 
               var fileSize = file.size; //Select the file size
               if (fileTypes.indexOf(fileType) == -1) { 
                       myalert.fire({
                        title: "Erreur",
                        text: `le format ${fileType} n'est pas autorisé utilisez le format .png`,
                        icon: "error",
                         button: "Ok"
                        });
                    return true;
               }

      if (+fileSize > 200*200) {
        myalert.fire({
          title: "Erreur",
          text: `importer un fichier de taille ${200}x${200} MG!`,
          icon: "error",
           button: "Ok"
          });
          return true;
      }

      var reader = new FileReader();
      reader.readAsDataURL(filePic);
      reader.onload =  (e:any) =>{
          var data = e.target.result;
                       
          var image = new Image();
          image.onload = ():any =>{
              var width = image.width;
              var height = image.height;
              if(width == 200 && height == 200) {                        
                return false ;
                 
              } else {
                   myalert.fire({
                    title: "Erreur",
                    text: `la taille doit etre ${200}x${200}`,
                    icon: "error",
                     button: "Ok"
                    });
                  return true ;
              }


          };
        };
  } else {

      return true;
      
  }

  return false;
}


process_front(e:any){
  if(e=="imgfront" && this.active){
    let objts=this.canvas.getObjects()
    if(objts.length>0){
      for(let i=0;i<objts.length;i++){
        this.canvas.remove(objts[i]);
      }
     this.canvas.backgroundImage=null;

    }
    if(this.front_data.length>0){ 
      for(let elt of this.front_data){
        this.canvas.add(elt);
        this.canvas.renderAll(elt);
      }
        
        }
    this.set(this.data.url);

    }
}


turnImage(event:any){
  let e=event.target.id;
 if(e=="imgback"){
  this.process_back(e);
 }
 if(e=="imgfront"){
  this.process_front(e);
 }
 
}


process_back(id:string){
  if(id=="imgback" && !this.active){
    let objts=this.canvas.getObjects();
    if(objts.length>0){
      for(let i=0;i<objts.length;i++){
        this.canvas.remove(objts[i]);
      }

    } 

    if(this.back_data.length>0){  
      for(let elt of this.back_data){
        this.canvas.add(elt);
        this.canvas.renderAll(elt);
      }     
    }
    this.set(this.data.url)
 

  }
  }



  set(url:any){
    var img = new Image();
    let canvas= this.canvas;
    img.src=url
    img.crossOrigin ='anonymous';
    
    img.onload = ()=> {
    var Img = new fabric.Image(img);
    canvas.setBackgroundImage(Img,canvas.renderAll.bind(canvas), {
            backgroundImageOpacity: 0.1,
            backgroundImageStretch: true,
            left:5,
            top:5,
            right:5,
            bottom:5,
            height:600,
            width:600,
        });
        canvas.centerObject(Img);
        canvas.renderAll(Img);
        canvas.requestRenderAll(); 
      }

  }


  savedesigns(){
    if(this.active){
      let items = this.canvas.getObjects();
      for(let el of items){
        this.front_data.push(el);
      }
        this.origin_1=this.canvas.toDataURL("png");
        this.face1=this.origin_1;
        this.active=!this.active;
        this.canvas.backgroundImage=null;
        this.production_f1=this.canvas.toDataURL()  
        var elt=document.getElementById('imgback');
        this.triggerMouse(elt);  
      
  
    }else{
      this.origin_2=this.canvas.toDataURL("png");
      this.face2=this.origin_2;
      let items = this.canvas.getObjects();
      for(let el of items){
        this.back_data.push(el);
      }
      this.canvas.backgroundImage=null;
      this.production_f2=this.canvas.toDataURL()
      this.active=!this.active;
      var elt=document.getElementById('imgfront');
      this.triggerMouse(elt);
    
   }

   this.saveIt();

  }


saveIt(){
if(this.origin_1 && this.origin_2){
  Object.assign(this.data,{face1:this.origin_1,face2:this.face2,text1:this.front_data,text2:this.back_data,production_f1:this.production_f1,production_f2:this.production_f2,category:"packs"});
 this.newdatapack=this.data;
  try{
    if(this.local.items.length>8){
     myalert.fire({
       title:"quota de stockage ",
       icon: 'info',
       html:"<h2 style='color:red;font-Size:12px'>Vous ne pouvez plus enregistrer de designs dans l'espace local, veuillez supprimer certains de vos designs ou passer au panier pour passer votre commande, afin de pouvoir poursuivre vos operations sur aladin.ci </h2>"
     }
     )
    }else{
     this.local.add(this.data);
    
    }
  }catch(err:any){
    if(err.code==22){
     myalert.fire({
       title:"quota de stockage ",
       icon: 'info',
       html:"<h2 style='color:red;font-Size:12px'>Vous ne pouvez plus enregistrer de designs dans l'espace local, veuillez supprimer certains de vos designs ou passer au panier pour passer votre commande, afin de pouvoir poursuivre vos operations sur aladin.ci </h2>"
     }
     )
       console.log(err);
    }else if(err=="QUOTA_EXCEEDED_ERR"){
     myalert.fire({
       title:"quota de stockage",
       icon:'info',
       html:"<h2 style='color:red;font-Size:12px'>Vous ne pouvez plus enregistrer de designs dans l'espace local, veuillez supprimer certains de vos designs ou passer au panier pour passer votre commande, afin de pouvoir poursuivre vos operations sur aladin.ci </h2>"
     }
     )
    }
}


}


}



}