import { Component, OnInit,Input } from '@angular/core';
import { LoginService,FormvalidationService ,AuthinfoService,LocalService,ListService} from 'src/app/core';
import { Router } from '@angular/router';
declare var require: any
var $ = require("jquery");
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
 data={
   email:"",
   password:"",
   erroremail:false,
   errorpwd:false,
   pwdmsg:"",
   emailmsg:""
 }
 URL="/users/"
 message="";
 user:any;
 name:any;
 ID:any
 @Input() cart_items:number=0;
 isauth=false;
 show:boolean=false;
 auth_data:any;
  constructor(private loginservice:LoginService,private valideform:FormvalidationService,private route:Router,private authuser:AuthinfoService,private ls :LocalService,private triger:ListService) { }

  ngOnInit(): void {
    $("body").children().first().before($(".modal"));

    this.cart_items=this.ls.cart_items.length;
    if(localStorage.getItem("token")){
      let token:any=localStorage.getItem('token'); 
      this.name=JSON.parse(token).user
      this.isauth=true;
      this.ID=JSON.parse(token).id;
     let now = new Date()

      if(now<token.expiryDate){
        this.user.name= token.user
        this.isauth=true;
        console.log(token.user,"ok")

      }else{
        console.log(token)
      }
     
    }

  }


  toggleSpinner(){
    this.show = !this.show;

  }


  login(event:Event){
    if(this.valideform.validateEmail(this.data.email)){
      if(this.data.password!=null){
        if(this.show){
          this.toggleSpinner()
        }
        this.toggleSpinner();
        this.loginservice.login(this.data).subscribe(res=>{
         console.log(res);
         if(res.user!=undefined){    
         // this.isauth=!this.isauth;
          this.user=res.user.data;
          this.name=this.user.name;
          this.ID=this.user.user_id;
          var now = new Date();
          this.isauth=true;
          let expiryDate = new Date(now.getTime() + res.token.exp*1000);
         this.auth_data={
           token:res.token.access_token,
           expiryDate:expiryDate,
           user:this.user.name,
           id:this.user.user_id
          }
          try{
            this.toggleSpinner();
            let id= document.getElementById("c");
            this.triger.triggerMouse(id)
            this.authuser.setItem('access_token',res.user.is_partner);
            this.authuser.setItem('user',res.user.id);
            localStorage.setItem('token',JSON.stringify(this.auth_data));
            

          }catch(err){
            console.log(err)
          }

         
         }
         this.toggleSpinner();
         this.data.erroremail=!this.data.erroremail;
         this.message=res.message;   
        
        },err=>{
          this.toggleSpinner();
           if(err.error.text!=undefined){
           this.data.erroremail=!this.data.erroremail;
           this.data.emailmsg=err.error.text;   
          }
          if(err.error.password!=undefined){
            this.data.errorpwd=!this.data.errorpwd;
            this.data.pwdmsg=err.error.password;   
          }
          if(this.data.erroremail!=true&&this.data.errorpwd!=true){
          console.log("Une erreur  s'est produite veuillez contactez le service client au 27 23 45 73 02")
          }   
        });
      }
    }else{
      this.data.emailmsg="invalide email";
      this.data.erroremail=!this.data.erroremail;

    }

  }

  logout(event:any){
     localStorage.removeItem('token');
     localStorage.removeItem('user');
     localStorage.removeItem('access_token')


     this.isauth=false;
     this.toggleSpinner();
     window.location.reload()
  }
 Scroll(){
   let view=document.getElementById('sommes')
   view?.scrollIntoView({behavior:'smooth'})
 }
}
