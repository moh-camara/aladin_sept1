import { Component, OnInit, Output,EventEmitter, Input } from '@angular/core';
import { charAtIndex } from 'pdf-lib';
import { LocalService } from 'src/app/core';
@Component({
  selector: 'app-cartitems',
  templateUrl: './cartitems.component.html',
  styleUrls: ['./cartitems.component.scss']
})
export class CartitemsComponent implements OnInit {
  cart_items:any;
  sub_total=0;
  id:any;
  isempty=true;
  diliver=0
  update=true;
  inpnmb=0;
  unit_price=1;
  price=0;
  editor_items:any=[];  
  crea_pack_items:any=[];
  crea_cloth_item:any=[];
  crea_disp_items:any=[];
  crea_print_items:any=[];
  crea_gadget_items:any=[]
  @Input() showbtn=true;
 @Output() mymoney=new EventEmitter<any>()
 
  lot:any=[]
  constructor(private item:LocalService) { }
  ngOnInit(): void {

    this.cart_items= this.item.cart_items;

    if(this.cart_items.length>0){
     this.isempty=!this.isempty;
 
     for(let item of this.cart_items){
 
       this.sub_total=this.sub_total+(parseInt(item.t));
 
       if(item.type_product=="crea" && item.category=="vetement"){
         
         item.size=JSON.parse(item.size)
         item.type=JSON.parse(item.type)
         item.type_shirt=JSON.parse(item.type_shirt)
         if(item.type.type=="Flexographie"){
         item.genre=JSON.parse(item.genre)
         item.photo=JSON.parse(item.photo);
         }
         this.crea_cloth_item.push(item);
       //  this.index_table.push(this.cart_items.indeOf(item))
 
       }
 
         if(item.type_product=="editor"){
           this.editor_items.push(item);
          // this.index_table.push(this.cart_items.indeOf(item))
 
         }
         if(item.type_product=="crea" && item.category=="emballage"){
           this.crea_pack_items.push(item);
           let lot=(item.qty/100)
           this.lot.push(lot);
           //this.index_table.push(this.cart_items.indeOf(item))
 
           
         }
        
   }
 
    
    }

  }

  removeItem(event:any){
    let id= event.target.id;
    let item:any;
    let id_cart:any= charAtIndex(id,0)
    let id_origin:any=charAtIndex(id,2);
    let category:any=id.slice(4)
     
    if(id_cart[0]!="-1"){
      item=this.cart_items[+id_cart[0]]
      this.sub_total=this.sub_total - parseInt(item.t)
      this.item.removeItem(+id_cart[0]);
      this.mymoney.emit(this.sub_total);
      if(category=="emballage"){

        let element:any= id_origin[0]
        console.log(this.crea_pack_items,element,id_origin)

        if(element!="-1"){
          element= parseInt(element)
         if(this.crea_pack_items.splice(element,element)){
          console.log(this.crea_pack_items)

         }

         if(+element==0){
          this.crea_pack_items=[]
        }
  
        }

      }

      if(category=="vetement"){
        let element:any= id_origin[0]
        if(+element==0){
          this.crea_cloth_item=[];
        }
        if(element!="-1"){
          this.crea_cloth_item.splice(+element,+element)
        }
      }

      if(category=="affichage"){
        let element:any= id_origin[0]
        if(+element==0){
          this.crea_cloth_item=[];
        }
        if(element!="-1"){
          this.crea_cloth_item.splice(+element,+element)
        }
      }

      if(category=="editor"){
        let element:any= id_origin[0]
        if(+element==0){
          this.editor_items=[]
        }
        if(element!="-1"){
          this.editor_items.splice(+element,+element)
        }else{
          console.log(element,id_origin[0]);
          console.log(this.editor_items)
        }
      }
    }

    
    
    if(this.cart_items.length==0){
      this.isempty=!this.isempty;

    }
  }


  emitMoney(value:any){
    this.mymoney.emit(value)
  }

  finale_step(event:any){
    location.href="/users/login"
  }



}
