import { createAttribute } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';
import Typed from 'typed.js'
@Component({
  selector: 'app-vetement',
  templateUrl: './vetement.component.html',
  styleUrls: ['./vetement.component.scss']
})
export class VetementComponent implements OnInit {
 cltobj:any=[]
 file:any
 imagepreview:any
 CacheCrea=false
 cacheClothes=true;
 details:any
 showdetail=false;
 url="/editor/cloth/"
 data="Vêtement : Aladin vous propose des vêtements personnalisés de très bonne qualité avec des finitions qui respectent vos goûts. Vous serez fière de les porter partout quel que soit le lieu et l’événement."
  constructor(private p:ListService) { }

  ngOnInit(): void {
    this.p.getcloths().subscribe(
      res=>{
      this.cltobj=res;
      this.cltobj=this.cltobj.data;
      ;

        },
      err=>{
        console.log(err)

      }

    )
    
  }
 Upload(event:any){
   let file =event.target.files[0]
   const reader = new FileReader();
 reader.onload = () => {
 
  this.imagepreview = reader.result;
  
  };
  
  reader.readAsDataURL(file);
  this.affichecrea()
  //let modal=document.getElementById('modalbtn')
  //this.p.triggerMouse(modal)
   console.log(event)

 }


showDetails(event:any){
  let id= event.target.id;
  if(id){

    for(let el of this.cltobj){
      this.details={id:id}

      id= +id;
      if(el.clt_id==id){
Object.assign(
  this.details,{url:el.img,comment:el.comment,price:el.price,size:JSON.parse(el.size),type:JSON.parse(el.type_imp),name:el.name_cloths,show:true});
  break;

      }
    }
  }
//console.log(this.details)
  this.CacheCrea=false
  this.cacheClothes=false;
}

affichecrea(){
  this.CacheCrea=true
  this.cacheClothes=false
}


getComponent(value:boolean){
  this.cacheClothes=value;
  this.CacheCrea=!this.CacheCrea;


}

ChangeComponent(value:boolean){
  this.cacheClothes=value
  
}

}
